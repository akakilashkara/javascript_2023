// const person = {firstName:"Akaki", lastName:"Lashkarashvili", age:19, eyeColor:"brown"};


const person = {
    firstName: "Akaki",
    lastName: "Lashkarashvili",
    age: 19,
    eyeColor: "brown"
  };

console.log(person.firstName)

console.log(person["age"])

const person1 = {
    firstName: "Akaki",
    lastName: "Lashkarashvili",
    age: 19,
    eyeColor: "brown",

    fullName: function(){
        return this.firstName + " " + this.lastName;
    }
  };

  console.log(person1.fullName())


function Person(first, last, age, eye) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.eyeColor = eye;
};

const me = new Person("Akaki", "Lashkarashvili", 19, "brown")
const brother = new Person("Luka", "Lashkarashvili", 13, "green") 

document.write("I am" + " " + me.age + ". my brother is" + " " + brother.age)


